"""aero_main URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin


urlpatterns = [
    url(r'^superadmin/', include(admin.site.urls)),
    url(r'^admin/', 'aero_app.views.login.login', name="login"),



    url(r'^accounts/auth/', 'aero_app.views.login.auth_view', name="auth"),
    url(r'^logout/', 'aero_app.views.login.logout_user', name="logout2"),

    url(r'^forbiden/', 'aero_app.views.administracion.forbiden', name="forbiden"),
    #se registra la url raiz del proyecto que hara redireccion al login
    url(r'^app/', 'aero_app.views.administracion.home', name="app"),
    #urls para la administracion de paises
    url(r'^paises/', 'aero_app.views.administracion.paises_list', name="paises"),
    url(r'^nuevopais/', 'aero_app.views.pais.createpais', name="paises"),
	url(r'^editarpais/(?P<id>\d+)/$', 'aero_app.views.pais.editpais', name="paises"),
	url(r'^eliminapais/(?P<id>\d+)/$', 'aero_app.views.pais.deletepais', name="paises"),

    #urls para la administracion de ciudades
    url(r'^ciudades/', 'aero_app.views.administracion.ciudades_list', name="ciudades"),
    url(r'^nuevociudad/', 'aero_app.views.ciudad.createciudad', name="ciudades1"),
	url(r'^editarciudad/(?P<id>\d+)/$', 'aero_app.views.ciudad.editciudad', name="ciudades2"),
	url(r'^eliminaciudad/(?P<id>\d+)/$', 'aero_app.views.ciudad.deleteciudad', name="ciudades3"),

    #urls para la administracion de aerolineas
    url(r'^aerolineas/', 'aero_app.views.administracion.aerolineas_list', name="aerolineas_list"),
    url(r'^nuevoaerolinea/', 'aero_app.views.aerolinea.createaerolinea', name="aerolineas1"),
	url(r'^editaraerolinea/(?P<id>\d+)/$', 'aero_app.views.aerolinea.editaerolinea', name="aerolineas2"),
	url(r'^eliminaaerolinea/(?P<id>\d+)/$', 'aero_app.views.aerolinea.deleteaerolinea', name="aerolineas3"),

    #urls para la administracion de aviones
    url(r'^aviones/', 'aero_app.views.administracion.aviones_list', name="aviones_list"),
    url(r'^nuevoavion/', 'aero_app.views.avion.createavion', name="aviones1"),
	url(r'^editaravion/(?P<id>\d+)/$', 'aero_app.views.avion.editavion', name="aviones2"),
	url(r'^eliminaavion/(?P<id>\d+)/$', 'aero_app.views.avion.deleteavion', name="aviones3"),

    #urls para la administracion de vuelos
    url(r'^vuelos/', 'aero_app.views.administracion.vuelos_list', name="vuelos_list"),
    url(r'^nuevovuelo/', 'aero_app.views.vuelo.createvuelo', name="vuelos1"),
	url(r'^editarvuelo/(?P<id>\d+)/$', 'aero_app.views.vuelo.editvuelo', name="vuelos2"),
	url(r'^eliminavuelo/(?P<id>\d+)/$', 'aero_app.views.vuelo.deletevuelo', name="vuelos3"),

    #urls para la administracion de tipo escala
    url(r'^escalas/', 'aero_app.views.administracion.escalas_list', name="escalas_list"),
    url(r'^nuevoescala/', 'aero_app.views.escala.createescala', name="escalas1"),
	url(r'^editarescala/(?P<id>\d+)/$', 'aero_app.views.escala.editescala', name="escalas2"),
	url(r'^eliminaescala/(?P<id>\d+)/$', 'aero_app.views.escala.deleteescala', name="escalas3"),

    #urls para la administracion de aeropuertos
    url(r'^aeropuertos/', 'aero_app.views.administracion.aeropuertos_list', name="aeropuertos_list"),
    url(r'^nuevoaeropuerto/', 'aero_app.views.aeropuerto.createaeropuerto', name="aeropuertos1"),
	url(r'^editaraeropuerto/(?P<id>\d+)/$', 'aero_app.views.aeropuerto.editaeropuerto', name="aeropuertos2"),
	url(r'^eliminaaeropuerto/(?P<id>\d+)/$', 'aero_app.views.aeropuerto.deleteaeropuerto', name="aeropuertos3"),

    #urls para la administracion de programas de vuelo
    url(r'^programas/', 'aero_app.views.administracion.programas_list', name="programas_list"),
    url(r'^nuevoprograma/', 'aero_app.views.programa.createprograma', name="programas1"),
	url(r'^editarprograma/(?P<id>\d+)/$', 'aero_app.views.programa.editprograma', name="programas2"),
	url(r'^eliminaprograma/(?P<id>\d+)/$', 'aero_app.views.programa.deleteprograma', name="programas3"),
    #urls prar los despegues de programas de vuelos
    url(r'^adddespegue/(?P<id>\d+)/$', 'aero_app.views.despegue.adddespegue', name="despegues"),
    url(r'^eliminadespegue/(?P<id>\d+)/(?P<id_programa>\d+)/$', 'aero_app.views.despegue.deletedespegue', name="despegues"),
    #urls prar los aterrizajes de programas de vuelos
    url(r'^addaterrizaje/(?P<id>\d+)/$', 'aero_app.views.aterrizaje.addaterrizaje', name="aterrizajes"),
    url(r'^eliminaaterrizaje/(?P<id>\d+)/(?P<id_programa>\d+)/$', 'aero_app.views.aterrizaje.deleteaterrizaje', name="aterrizajes"),
    #urls prar las escalas tecnicas de programas de vuelos
    url(r'^addescalastecnica/(?P<id>\d+)/$', 'aero_app.views.escala_tecnica.addescalastecnica', name="escalastecnicas"),
    url(r'^eliminaescalastecnica/(?P<id>\d+)/(?P<id_programa>\d+)/$', 'aero_app.views.escala_tecnica.deleteescalastecnica', name="escalastecnicas"),
    #urls prar los dias de programas de vuelos
    url(r'^adddia/(?P<id>\d+)/$', 'aero_app.views.dia.adddia', name="dias"),
    url(r'^eliminadia/(?P<id>\d+)/(?P<id_programa>\d+)/$', 'aero_app.views.dia.deletedia', name="dias"),
    #urls prar los horarios del dia de programas de vuelos
    url(r'^addhora/(?P<id>\d+)/$', 'aero_app.views.hora.addhora', name="horas"),
    url(r'^eliminahora/(?P<id>\d+)/(?P<id_dia>\d+)/$', 'aero_app.views.hora.deletehora', name="horas"),

    #urls para la administracion de clases
    url(r'^clases/', 'aero_app.views.administracion.clases_list', name="clases"),
    url(r'^nuevoclase/', 'aero_app.views.clase.createclase', name="clases"),
	url(r'^editarclase/(?P<id>\d+)/$', 'aero_app.views.clase.editclase', name="clases"),
	url(r'^eliminaclase/(?P<id>\d+)/$', 'aero_app.views.clase.deleteclase', name="clases"),

    #urls prar los precios de programas de vuelos
    url(r'^addprecio/(?P<id>\d+)/$', 'aero_app.views.precio.addprecio', name="precios"),
    url(r'^eliminaprecio/(?P<id>\d+)/(?P<id_programa>\d+)/$', 'aero_app.views.precio.deleteprecio', name="precios"),

    #urls app clientes
    url(r'^$', 'aero_app.views.search.main', name="results"),
    url(r'^results/', 'aero_app.views.search.get_vuelos', name="results"),

    #urls para que el cliente se registre usando la aplicacion django-registration
    url(r'^accounts/', include('registration.backends.simple.urls')),

]
