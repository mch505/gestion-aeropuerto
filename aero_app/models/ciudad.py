from django.db import models
from .pais import Pais


class Ciudad(models.Model):
    pais = models.ForeignKey(Pais, related_name='paises_ciudad',verbose_name="Pais")
    nombre = models.CharField(max_length=200, null=True ,verbose_name="Nombre Ciudad")
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.nombre
