from django.db import models
from .modeloavion import ModeloAvion


class Vuelos(models.Model):
    avion = models.ForeignKey(ModeloAvion, related_name='aviones')
    numero_vuelo = models.IntegerField(null=True)
    plazas_vacias = models.IntegerField(null=True)
    fecha_vuelo = models.DateField(null=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return str(self.numero_vuelo)
