from django.db import models


class Clase(models.Model):
    nombre = models.CharField(max_length=200, null=True ,verbose_name="Nombre Clase")
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.nombre
