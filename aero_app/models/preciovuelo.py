from django.db import models
from .programa import ProgramaVuelo
from .clase import Clase
from .programa import ProgramaVuelo

class PrecioVuelo(models.Model):
    clase = models.ForeignKey(Clase, related_name='precios')
    programa = models.ForeignKey(ProgramaVuelo, related_name='precios')
    precio = models.DecimalField(max_digits=8, decimal_places=2, null=True)
    created = models.DateTimeField(auto_now_add=True,null=True)
