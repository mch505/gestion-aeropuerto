from django.db import models
from .diasprograma import DiasPrograma


class HorarioPrograma(models.Model):
    dia = models.ForeignKey(DiasPrograma, related_name='horas')
    hora = models.TimeField(null=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.hora
