from django.db import models
from .programa import ProgramaVuelo
from .aeropuerto import Aeropuerto
from .tipoescala import TipoEscala

class EscalasTecnicas(models.Model):
    descripcion = models.CharField(max_length=200, null=True)
    programa = models.ForeignKey(ProgramaVuelo, related_name='escalas')
    aeropuerto = models.ForeignKey(Aeropuerto, related_name='escalas')
    tipo_escala = models.ForeignKey(TipoEscala, related_name='escalas')
    created = models.DateTimeField(auto_now_add=True)
