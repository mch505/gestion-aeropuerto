from django.db import models
from .programa import ProgramaVuelo
from .aeropuerto import Aeropuerto

class Aterrizajes(models.Model):
    programa = models.ForeignKey(ProgramaVuelo, related_name='aterrizajes')
    aeropuerto = models.ForeignKey(Aeropuerto, related_name='aterrizajes')
    created = models.DateTimeField(auto_now_add=True)
