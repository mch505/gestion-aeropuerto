from django.db import models
from .ciudad import Ciudad


class Aeropuerto(models.Model):
    ciudad = models.ForeignKey(Ciudad, related_name='ciudades_aeropuerto')
    codigo = models.CharField(max_length=200, null=True)
    nombre = models.CharField(max_length=200, null=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.nombre
