from django.db import models
from .programa import ProgramaVuelo
from .aeropuerto import Aeropuerto

class Despegues(models.Model):
    programa = models.ForeignKey(ProgramaVuelo, related_name='despegues')
    aeropuerto = models.ForeignKey(Aeropuerto, related_name='despegues')
    created = models.DateTimeField(auto_now_add=True)
