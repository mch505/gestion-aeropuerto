from .aerolinea import Aerolinea
from .aeropuerto import Aeropuerto

from .ciudad import Ciudad

from .modeloavion import ModeloAvion
from .pais import Pais

from .programa import ProgramaVuelo
from .vuelos import Vuelos


from .diasprograma import DiasPrograma
from .horarioprograma import HorarioPrograma

from .despegues import Despegues
from .aterrizajes import Aterrizajes

from .tipoescala import TipoEscala
from .escalastecnicas import EscalasTecnicas

from .perfil import Perfil
from .clase import Clase

from .preciovuelo import PrecioVuelo
