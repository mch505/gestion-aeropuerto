from django.db import models
from .aeropuerto import Aeropuerto
from django.contrib.auth.models import User


class Perfil(models.Model):
    user = models.OneToOneField(User, related_name='perfil')
    aeropuerto = models.ForeignKey(Aeropuerto)
    identificacion = models.CharField(max_length=50,null=True)
    direccion = models.CharField(max_length=150)
    telefono = models.CharField(max_length=15)


    def __unicode__(self):
        return u"Aeropuerto {}:{}".format(self.aeropuerto.pk, self.aeropuerto.nombre)
