from django.db import models


class ModeloAvion(models.Model):
    descripcion = models.CharField(max_length=200, null=True)
    capacidad = models.IntegerField(null=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.descripcion
