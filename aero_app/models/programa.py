from django.db import models
from .vuelos import Vuelos
from .aerolinea import Aerolinea


class ProgramaVuelo(models.Model):
    vuelo = models.ForeignKey(Vuelos, related_name='programas')
    aerolinea = models.ForeignKey(Aerolinea, related_name='programas')
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
