from django.db import models
from .programa import ProgramaVuelo


class DiasPrograma(models.Model):
    STATUS_OPTIONS = (
        ('Lunes', 'Lunes'),
        ('Martes', 'Martes'),
        ('Miercoles', 'Miercoles'),
        ('Jueves', 'Jueves'),
        ('Viernes', 'Viernes'),
        ('Sabado', 'Sabado'),
        ('Domingo', 'Domingo'),
    )
    programa = models.ForeignKey(ProgramaVuelo, related_name='dias')
    dias_semana = models.CharField(max_length=20, choices = STATUS_OPTIONS ,default = STATUS_OPTIONS[0][0])
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
