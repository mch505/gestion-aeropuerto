from django import forms
from django.contrib.auth.models import *
from aero_app.models import *
from datetimewidget.widgets import DateWidget
from datetimewidget.widgets import TimeWidget

class PaisForm(forms.ModelForm):
    class Meta:
        model = Pais
        exclude=('created','modified')

class CiudadForm(forms.ModelForm):
    class Meta:
        model = Ciudad
        exclude=('created','modified')

class AerolineaForm(forms.ModelForm):
    class Meta:
        model = Aerolinea
        exclude=('created','modified')

class AvionForm(forms.ModelForm):
    class Meta:
        model = ModeloAvion
        exclude=('created','modified')

class VueloForm(forms.ModelForm):

    class Meta:
        model = Vuelos
        widgets = {
            'fecha_vuelo': DateWidget(usel10n=True, bootstrap_version=3)
        }
        exclude=('created','modified','numero_vuelo')

class EscalaForm(forms.ModelForm):
    class Meta:
        model = TipoEscala
        exclude=('created','modified')

class AeropuertoForm(forms.ModelForm):
    class Meta:
        model = Aeropuerto
        exclude=('created','modified')

class UsuarioForm(forms.ModelForm):
    class Meta:
        model = User
        exclude=('email','is_staff','is_superuser','last_login','date_joined')

class ProgramaForm(forms.ModelForm):
    class Meta:
        model = ProgramaVuelo
        exclude=('created','modified')

class DespegueFrom(forms.ModelForm):
    class Meta:
        model = Despegues
        exclude=('created','programa')

class AterrizajeFrom(forms.ModelForm):
    class Meta:
        model = Aterrizajes
        exclude=('created','programa')

class EscalaTecnicaFrom(forms.ModelForm):
    class Meta:
        model = EscalasTecnicas
        exclude=('created','programa')

class DiasProgramaFrom(forms.ModelForm):
    class Meta:
        model = DiasPrograma
        exclude=('created','programa')

class HorarioProgramaForm(forms.ModelForm):
    class Meta:
        model = HorarioPrograma
        widgets = {
            'hora': TimeWidget(usel10n=True, bootstrap_version=3)
        }
        exclude=('created','programa','dia')

class ClaseForm(forms.ModelForm):
    class Meta:
        model = Clase
        exclude=('created','modified')

class PrecioForm(forms.ModelForm):
    class Meta:
        model = PrecioVuelo
        exclude=('created','programa')
