# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aero_app', '0003_auto_20151109_0901'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='empleado',
            name='aeropuerto',
        ),
        migrations.RemoveField(
            model_name='empleado',
            name='users',
        ),
        migrations.DeleteModel(
            name='Empleado',
        ),
    ]
