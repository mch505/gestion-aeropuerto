# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aero_app', '0006_auto_20151110_2200'),
    ]

    operations = [
        migrations.AlterField(
            model_name='diasprograma',
            name='dias_semana',
            field=models.CharField(default=b'Monday', max_length=20, choices=[(b'Monday', b'Lunes'), (b'Tuesday', b'Martes'), (b'Wednesday', b'Miercoles'), (b'Thursday', b'Jueves')]),
        ),
    ]
