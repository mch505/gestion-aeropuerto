# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Aerolinea',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=200, null=True)),
                ('is_active', models.BooleanField(default=True, editable=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Aeropuerto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('codigo', models.CharField(max_length=200, null=True)),
                ('nombre', models.CharField(max_length=200, null=True)),
                ('is_active', models.BooleanField(default=True, editable=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Aterrizajes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('aeropuerto', models.ForeignKey(related_name='aeropuertos_aterrizajes', to='aero_app.Aeropuerto')),
            ],
        ),
        migrations.CreateModel(
            name='Ciudad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=200, null=True, verbose_name=b'Nombre Ciudad')),
                ('is_active', models.BooleanField(default=True, editable=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Despegues',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('aeropuerto', models.ForeignKey(related_name='aeropuertos_despegues', to='aero_app.Aeropuerto')),
            ],
        ),
        migrations.CreateModel(
            name='DiasPrograma',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dias_semana', models.CharField(max_length=20, null=True)),
                ('is_active', models.BooleanField(default=True, editable=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Empleado',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=200, null=True)),
                ('apellido', models.CharField(max_length=200, null=True)),
                ('telefono', models.CharField(max_length=20, null=True)),
                ('is_active', models.BooleanField(default=True, editable=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('aeropuerto', models.ForeignKey(related_name='aeropuerto_empleados', to='aero_app.Aeropuerto')),
                ('users', models.ForeignKey(related_name='usuarios_empleados', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='EscalasTecnicas',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('descripcion', models.CharField(max_length=200, null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('aeropuerto', models.ForeignKey(related_name='aeropuertos_escalas', to='aero_app.Aeropuerto')),
            ],
        ),
        migrations.CreateModel(
            name='HorarioPrograma',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('hora', models.TimeField(null=True)),
                ('is_active', models.BooleanField(default=True, editable=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('dia', models.ForeignKey(related_name='dias_horario', to='aero_app.DiasPrograma')),
            ],
        ),
        migrations.CreateModel(
            name='ModeloAvion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('descripcion', models.CharField(max_length=200, null=True)),
                ('capacidad', models.IntegerField(null=True)),
                ('is_active', models.BooleanField(default=True, editable=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Pais',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=200, null=True, verbose_name=b'Nombre Pais')),
                ('is_active', models.BooleanField(default=True, editable=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='ProgramaVuelo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_active', models.BooleanField(default=True, editable=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('aerolinea', models.ForeignKey(related_name='aerolineas_programas', to='aero_app.Aerolinea')),
            ],
        ),
        migrations.CreateModel(
            name='TipoEscala',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('descripcion', models.CharField(max_length=200, null=True)),
                ('is_active', models.BooleanField(default=True, editable=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Vuelos',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('numero_vuelo', models.IntegerField(null=True)),
                ('ciudad_despegue', models.CharField(max_length=200, null=True)),
                ('ciudad_destino', models.CharField(max_length=200, null=True)),
                ('plazas_vacias', models.IntegerField(null=True)),
                ('fecha_vuelo', models.DateField(null=True)),
                ('is_active', models.BooleanField(default=True, editable=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('avion', models.ForeignKey(related_name='aviones', to='aero_app.ModeloAvion')),
            ],
        ),
        migrations.AddField(
            model_name='programavuelo',
            name='vuelo',
            field=models.ForeignKey(related_name='vuelos_programas', to='aero_app.Vuelos'),
        ),
        migrations.AddField(
            model_name='escalastecnicas',
            name='programa',
            field=models.ForeignKey(related_name='programas_escalas', to='aero_app.ProgramaVuelo'),
        ),
        migrations.AddField(
            model_name='escalastecnicas',
            name='tipo_escala',
            field=models.ForeignKey(related_name='aeropuertos_escalas', to='aero_app.TipoEscala'),
        ),
        migrations.AddField(
            model_name='diasprograma',
            name='programa',
            field=models.ForeignKey(related_name='programas_dias', to='aero_app.ProgramaVuelo'),
        ),
        migrations.AddField(
            model_name='despegues',
            name='programa',
            field=models.ForeignKey(related_name='programas_despegues', to='aero_app.ProgramaVuelo'),
        ),
        migrations.AddField(
            model_name='ciudad',
            name='pais',
            field=models.ForeignKey(related_name='paises_ciudad', verbose_name=b'Pais', to='aero_app.Pais'),
        ),
        migrations.AddField(
            model_name='aterrizajes',
            name='programa',
            field=models.ForeignKey(related_name='programas_aterrizajes', to='aero_app.ProgramaVuelo'),
        ),
        migrations.AddField(
            model_name='aeropuerto',
            name='ciudad',
            field=models.ForeignKey(related_name='ciudades_aeropuerto', to='aero_app.Ciudad'),
        ),
        migrations.AddField(
            model_name='aerolinea',
            name='pais',
            field=models.ForeignKey(related_name='paises_aerolinea', to='aero_app.Pais'),
        ),
    ]
