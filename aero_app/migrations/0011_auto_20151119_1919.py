# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aero_app', '0010_auto_20151116_1944'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='preciovuelo',
            name='vuelo',
        ),
        migrations.AddField(
            model_name='preciovuelo',
            name='programa',
            field=models.ForeignKey(related_name='precios', to='aero_app.ProgramaVuelo', null=True),
        ),
        migrations.AlterField(
            model_name='preciovuelo',
            name='created',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
    ]
