# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aero_app', '0007_auto_20151110_2223'),
    ]

    operations = [
        migrations.AlterField(
            model_name='diasprograma',
            name='dias_semana',
            field=models.CharField(default=b'Lunes', max_length=20, choices=[(b'Lunes', b'Lunes'), (b'Martes', b'Martes'), (b'Miercoles', b'Miercoles'), (b'Jueves', b'Jueves'), (b'Viernes', b'Viernes'), (b'Sabado', b'Sabado'), (b'Domingo', b'Domingo')]),
        ),
    ]
