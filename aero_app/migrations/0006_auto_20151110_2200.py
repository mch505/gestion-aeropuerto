# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aero_app', '0005_auto_20151109_1005'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='aerolinea',
            name='is_active',
        ),
        migrations.RemoveField(
            model_name='aeropuerto',
            name='is_active',
        ),
        migrations.RemoveField(
            model_name='ciudad',
            name='is_active',
        ),
        migrations.RemoveField(
            model_name='diasprograma',
            name='is_active',
        ),
        migrations.RemoveField(
            model_name='horarioprograma',
            name='is_active',
        ),
        migrations.RemoveField(
            model_name='modeloavion',
            name='is_active',
        ),
        migrations.RemoveField(
            model_name='pais',
            name='is_active',
        ),
        migrations.RemoveField(
            model_name='perfil',
            name='apellido',
        ),
        migrations.RemoveField(
            model_name='perfil',
            name='nombre',
        ),
        migrations.RemoveField(
            model_name='programavuelo',
            name='is_active',
        ),
        migrations.RemoveField(
            model_name='tipoescala',
            name='is_active',
        ),
        migrations.RemoveField(
            model_name='vuelos',
            name='is_active',
        ),
    ]
