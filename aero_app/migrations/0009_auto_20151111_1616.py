# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aero_app', '0008_auto_20151110_2231'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aterrizajes',
            name='aeropuerto',
            field=models.ForeignKey(related_name='aterrizajes', to='aero_app.Aeropuerto'),
        ),
        migrations.AlterField(
            model_name='aterrizajes',
            name='programa',
            field=models.ForeignKey(related_name='aterrizajes', to='aero_app.ProgramaVuelo'),
        ),
        migrations.AlterField(
            model_name='despegues',
            name='aeropuerto',
            field=models.ForeignKey(related_name='despegues', to='aero_app.Aeropuerto'),
        ),
        migrations.AlterField(
            model_name='despegues',
            name='programa',
            field=models.ForeignKey(related_name='despegues', to='aero_app.ProgramaVuelo'),
        ),
    ]
