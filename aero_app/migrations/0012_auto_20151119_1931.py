# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aero_app', '0011_auto_20151119_1919'),
    ]

    operations = [
        migrations.AlterField(
            model_name='preciovuelo',
            name='programa',
            field=models.ForeignKey(related_name='precios', to='aero_app.ProgramaVuelo'),
        ),
    ]
