# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aero_app', '0004_auto_20151109_0901'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='vuelos',
            name='ciudad_despegue',
        ),
        migrations.RemoveField(
            model_name='vuelos',
            name='ciudad_destino',
        ),
    ]
