# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aero_app', '0009_auto_20151111_1616'),
    ]

    operations = [
        migrations.CreateModel(
            name='Clase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=200, null=True, verbose_name=b'Nombre Clase')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='PrecioVuelo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('precio', models.DecimalField(null=True, max_digits=8, decimal_places=2)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('clase', models.ForeignKey(related_name='precios', to='aero_app.Clase')),
                ('vuelo', models.ForeignKey(related_name='precios', to='aero_app.Vuelos')),
            ],
        ),
        migrations.AlterField(
            model_name='diasprograma',
            name='programa',
            field=models.ForeignKey(related_name='dias', to='aero_app.ProgramaVuelo'),
        ),
        migrations.AlterField(
            model_name='escalastecnicas',
            name='aeropuerto',
            field=models.ForeignKey(related_name='escalas', to='aero_app.Aeropuerto'),
        ),
        migrations.AlterField(
            model_name='escalastecnicas',
            name='programa',
            field=models.ForeignKey(related_name='escalas', to='aero_app.ProgramaVuelo'),
        ),
        migrations.AlterField(
            model_name='escalastecnicas',
            name='tipo_escala',
            field=models.ForeignKey(related_name='escalas', to='aero_app.TipoEscala'),
        ),
        migrations.AlterField(
            model_name='horarioprograma',
            name='dia',
            field=models.ForeignKey(related_name='horas', to='aero_app.DiasPrograma'),
        ),
        migrations.AlterField(
            model_name='programavuelo',
            name='aerolinea',
            field=models.ForeignKey(related_name='programas', to='aero_app.Aerolinea'),
        ),
        migrations.AlterField(
            model_name='programavuelo',
            name='vuelo',
            field=models.ForeignKey(related_name='programas', to='aero_app.Vuelos'),
        ),
    ]
