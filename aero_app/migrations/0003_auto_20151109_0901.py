# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aero_app', '0002_perfil'),
    ]

    operations = [
        migrations.AddField(
            model_name='perfil',
            name='apellido',
            field=models.CharField(max_length=150, null=True),
        ),
        migrations.AddField(
            model_name='perfil',
            name='identificacion',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='perfil',
            name='nombre',
            field=models.CharField(max_length=150, null=True),
        ),
    ]
