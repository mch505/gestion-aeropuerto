from permission.logics import AuthorPermissionLogic
from permission.logics import CollaboratorsPermissionLogic


PERMISSION_LOGICS = (
    ('aero_app.Aerolinea', AuthorPermissionLogic()),
    ('aero_app.Aeropuerto', CollaboratorsPermissionLogic()),
)
