from django.http import *
from django.shortcuts import render_to_response,redirect,render
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import *
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from aero_app.forms import AeropuertoForm
from aero_app.models import *
from django.contrib.auth.decorators import permission_required

@login_required(login_url='/admin') # este decorador obliga a que el usuario este logueado para acceder a la url
@permission_required('aero_app.add_aeropuerto', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def createaeropuerto(request,template_name='administracion/aeropuerto/new.html'):
    if request.method == "POST":
        form =AeropuertoForm(request.POST)
        if form.is_valid():
            aeropuerto, created = Aeropuerto.objects.get_or_create(**form.cleaned_data)
            if(created == True):
                aeropuerto.save()
                return HttpResponseRedirect("/aeropuertos/")
            else:
                return render_to_response(template_name, {
                    'form': form,
                    'messages' : 'Error: ya existe el registro, por favor ingrese otro'
                }, context_instance=RequestContext(request))
    else:
        form =AeropuertoForm()

    return render_to_response(template_name, {
        'form': form,
    }, context_instance=RequestContext(request))

@login_required(login_url='/admin')
@permission_required('aero_app.change_aeropuerto', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def editaeropuerto(request, id=None, template_name='administracion/aeropuerto/new.html'):
    if id:
        aeropuerto = get_object_or_404(Aeropuerto, pk=id)
    else:
        aeropuerto =Aeropuerto.objects.filter(pk=id)
    if request.POST:
        form =AeropuertoForm(request.POST, instance=aeropuerto)
        if form.is_valid():
            form.save(commit=False)
            form.save()
            return HttpResponseRedirect("/aeropuertos/")
    else:
        form =AeropuertoForm(instance=aeropuerto)

    return render_to_response(template_name, {
        'form': form,
    }, context_instance=RequestContext(request))

@login_required(login_url='/admin')
@permission_required('aero_app.delete_aeropuerto', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def deleteaeropuerto(request, id = None):
    if id:
        aeropuerto = get_object_or_404(Aeropuerto, pk=id)
        aeropuerto.delete();
        return HttpResponseRedirect("/aeropuertos/")
    else:
        aeropuerto =Aeropuerto(pk=id)
