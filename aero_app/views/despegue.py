from django.http import *
from django.shortcuts import render_to_response,redirect,render
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import *
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from aero_app.forms import DespegueFrom
from aero_app.models import *
from django.contrib.auth.decorators import permission_required

@login_required(login_url='/admin')
@permission_required('aero_app.add_despegues', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def adddespegue(request,id=None,template_name='administracion/programas/despegues/add.html'):
    if request.method == "POST":
        form =DespegueFrom(request.POST)
        if form.is_valid():
            despegue = form.save(commit=False)
            despegue.programa_id = id
            despegue.save()
            despegues = Despegues.objects.filter(programa_id = id)
            return render_to_response(template_name, {
                'form': form,
                'despegues': despegues
            }, context_instance=RequestContext(request))
    else:
        form = DespegueFrom()

    despegues = Despegues.objects.filter(programa_id = id)
    return render_to_response(template_name, {
        'form': form,
        'despegues': despegues
    }, context_instance=RequestContext(request))


@login_required(login_url='/admin')
@permission_required('aero_app.delete_despegues', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def deletedespegue(request, id = None,id_programa = None):
    if id:
        despegue = get_object_or_404(Despegues, pk=id)
        despegue.delete();
        form = DespegueFrom()
        return HttpResponseRedirect("/adddespegue/"+id_programa+"/")
    else:
        despegue =Despegues(pk=id)
