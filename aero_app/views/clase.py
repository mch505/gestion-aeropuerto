from django.http import *
from django.shortcuts import render_to_response,redirect,render
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import *
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from aero_app.forms import ClaseForm
from aero_app.models import *
from django.contrib.auth.decorators import permission_required

@login_required(login_url='/admin')
@permission_required('aero_app.add_clase', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def createclase(request,template_name='administracion/clases/new.html'):
    if request.method == "POST":
        form = ClaseForm(request.POST)
        if form.is_valid():
            clase = form.save(commit=False)
            clase.save()
            return HttpResponseRedirect("/clases/")
    else:
        form = ClaseForm()

    return render_to_response(template_name, {
        'form': form,
    }, context_instance=RequestContext(request))

@login_required(login_url='/admin')
@permission_required('aero_app.change_clase', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def editclase(request, id=None, template_name='administracion/clases/new.html'):
    if id:
        clase = get_object_or_404(Clase, pk=id)
    else:
        clase = Clase.objects.filter(pk=id)
    if request.POST:
        form = ClaseForm(request.POST, instance=clase)
        if form.is_valid():
            form.save(commit=False)
            form.save()
            return HttpResponseRedirect("/clases/")
    else:
        form = ClaseForm(instance=clase)

    return render_to_response(template_name, {
        'form': form,
    }, context_instance=RequestContext(request))

@login_required(login_url='/admin')
@permission_required('aero_app.delete_clase', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def deleteclase(request, id = None):
    if id:
        clase = get_object_or_404(Clase, pk=id)
        clase.delete();
        return HttpResponseRedirect("/clases/")
    else:
        user = Clase(pk=id)
