from django.http import *
from django.shortcuts import render_to_response,redirect,render
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import *
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from aero_app.forms import CiudadForm
from aero_app.models import *
from django.contrib.auth.decorators import permission_required

@login_required(login_url='/admin')
@permission_required('aero_app.add_ciudad', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def createciudad(request,template_name='administracion/ciudades/new.html'):
    if request.method == "POST":
        form = CiudadForm(request.POST)
        if form.is_valid():
            ciudad, created = Ciudad.objects.get_or_create(**form.cleaned_data)
            if(created == True):
                ciudad.save()
                return HttpResponseRedirect("/ciudades/")
            else:
                return render_to_response(template_name, {
                    'form': form,
                    'messages' : 'Error: ya existe el registro, por favor ingrese otro'
                }, context_instance=RequestContext(request))
    else:
        form = CiudadForm()

    return render_to_response(template_name, {
        'form': form,
    }, context_instance=RequestContext(request))


@login_required(login_url='/admin')
@permission_required('aero_app.change_ciudad', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def editciudad(request, id=None, template_name='administracion/ciudades/new.html'):
    if id:
        ciudad = get_object_or_404(Ciudad, pk=id)
    else:
        ciudad = Ciudad.objects.filter(pk=id)
    if request.POST:
        form = CiudadForm(request.POST, instance=ciudad)
        if form.is_valid():
            form.save(commit=False)
            form.save()
            return HttpResponseRedirect("/ciudades/")
    else:
        form = CiudadForm(instance=ciudad)

    return render_to_response(template_name, {
        'form': form,
    }, context_instance=RequestContext(request))


@login_required(login_url='/admin')
@permission_required('aero_app.delete_ciudad', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def deleteciudad(request, id = None):
    if id:
        ciudad = get_object_or_404(Ciudad, pk=id)
        ciudad.delete();
        return HttpResponseRedirect("/ciudades/")
    else:
        user = Ciudad(pk=id)
