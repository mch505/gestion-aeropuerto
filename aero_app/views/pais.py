from django.http import *
from django.shortcuts import render_to_response,redirect,render
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import *
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from aero_app.forms import PaisForm
from aero_app.models import *
from django.contrib.auth.decorators import permission_required

@login_required(login_url='/admin')
@permission_required('aero_app.add_pais', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def createpais(request,template_name='administracion/paises/new.html'):
    if request.method == "POST":
        form = PaisForm(request.POST)
        if form.is_valid():
            pais, created = Pais.objects.get_or_create(**form.cleaned_data)
            if(created == True):
                pais.save()
                return HttpResponseRedirect("/paises/")
            else:
                return render_to_response(template_name, {
                    'form': form,
                    'messages' : 'Error: ya existe el registro, por favor ingrese otro'
                }, context_instance=RequestContext(request))
    else:
        form = PaisForm()

    return render_to_response(template_name, {
        'form': form,
    }, context_instance=RequestContext(request))

@login_required(login_url='/admin')
@permission_required('aero_app.change_pais', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def editpais(request, id=None, template_name='administracion/paises/new.html'):
    if id:
        pais = get_object_or_404(Pais, pk=id)
    else:
        pais = Pais.objects.filter(pk=id)
    if request.POST:
        form = PaisForm(request.POST, instance=pais)
        if form.is_valid():
            form.save(commit=False)
            form.save()
            return HttpResponseRedirect("/paises/")
    else:
        form = PaisForm(instance=pais)

    return render_to_response(template_name, {
        'form': form,
    }, context_instance=RequestContext(request))

@login_required(login_url='/admin')
@permission_required('aero_app.delete_pais', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def deletepais(request, id = None):
    if id:
        pais = get_object_or_404(Pais, pk=id)
        pais.delete();
        return HttpResponseRedirect("/paises/")
    else:
        user = Pais(pk=id)
