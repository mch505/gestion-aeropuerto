# -*- encoding: utf-8 -*-
from django.http import *
from django.shortcuts import render_to_response,redirect,render
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.core.context_processors import csrf
from django.contrib import auth
from django.contrib.auth import logout


def login(request):
    c = {}
    c.update(csrf(request))
    return render(request,'login.html', {'var':'hidden'})

def auth_view(request):
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    user = auth.authenticate(username = username, password = password)

    if user is not None:
        if(user.is_staff):
            auth.login(request,user)
            return HttpResponseRedirect('/app/')
        else:
            c = {}
            c.update(csrf(request))
            return render(request,'login.html', {'var':'hidden', "messages": "El usuario no es parte del staff de administración"})
    else:
        return HttpResponseRedirect('/admin/')

def logout_user(request):
    logout(request)
    return HttpResponseRedirect("/admin/")
