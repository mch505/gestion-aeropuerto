from django.http import *
from django.shortcuts import render_to_response,redirect,render
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import *
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from aero_app.forms import VueloForm
from aero_app.models import *
import uuid
from django.contrib.auth.decorators import permission_required

@login_required(login_url='/admin')
@permission_required('aero_app.add_vuelo', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def createvuelo(request,template_name='administracion/vuelos/new.html'):
    if request.method == "POST":
        form =VueloForm(request.POST)
        if form.is_valid():
            vuelo, created = Vuelos.objects.get_or_create(**form.cleaned_data) #esta funcion del orm de django lo que hace es verificar si el registo existe en base de datos y si no lo crea
            if(created == True):
                vuelo.numero_vuelo = hash(str(uuid.uuid1())) % 1000000
                vuelo.save()
                return HttpResponseRedirect("/vuelos/")
            else:
                return render_to_response(template_name, {
                    'form': form,
                    'messages' : 'Error: ya existe el registro, por favor ingrese otro'
                }, context_instance=RequestContext(request))
    else:
        aviones = ModeloAvion.objects.all()
        form =VueloForm()

    return render_to_response(template_name, {
        'form': form,
        'aviones': aviones
    }, context_instance=RequestContext(request))

@login_required(login_url='/admin')
@permission_required('aero_app.change_vuelo', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def editvuelo(request, id=None, template_name='administracion/vuelos/new.html'):
    if id:
        vuelo = get_object_or_404(Vuelos, pk=id)
    else:
        vuelo =Vuelos.objects.filter(pk=id)
    if request.POST:
        form =VueloForm(request.POST, instance=vuelo)
        if form.is_valid():
            form.save(commit=False)
            form.save()
            return HttpResponseRedirect("/vuelos/")
    else:
        form =VueloForm(instance=vuelo)

    return render_to_response(template_name, {
        'form': form,
    }, context_instance=RequestContext(request))

@login_required(login_url='/admin')
@permission_required('aero_app.delete_vuelo', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def deletevuelo(request, id = None):
    if id:
        vuelo = get_object_or_404(Vuelos, pk=id)
        vuelo.delete();
        return HttpResponseRedirect("/vuelos/")
    else:
        vuelo =Vuelo(pk=id)
