from django.http import *
from django.shortcuts import render_to_response,redirect,render
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import *
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from aero_app.forms import AterrizajeFrom
from aero_app.models import *
from django.contrib.auth.decorators import permission_required

@login_required(login_url='/admin')
@permission_required('aero_app.add_aterrizajes', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def addaterrizaje(request,id=None,template_name='administracion/programas/aterrizajes/add.html'):
    if request.method == "POST":
        form =AterrizajeFrom(request.POST)
        if form.is_valid():
            aterrizaje = form.save(commit=False)
            aterrizaje.programa_id = id
            aterrizaje.save()
            aterrizajes = Aterrizajes.objects.filter(programa_id = id)
            return render_to_response(template_name, {
                'form': form,
                'aterrizajes': aterrizajes
            }, context_instance=RequestContext(request))
    else:
        form = AterrizajeFrom()

    aterrizajes = Aterrizajes.objects.filter(programa_id = id)
    return render_to_response(template_name, {
        'form': form,
        'aterrizajes': aterrizajes
    }, context_instance=RequestContext(request))


@login_required(login_url='/admin')
@permission_required('aero_app.delete_aterrizajes', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def deleteaterrizaje(request, id = None,id_programa = None):
    if id:
        aterrizaje = get_object_or_404(Aterrizajes, pk=id)
        aterrizaje.delete();
        form = AterrizajeFrom()
        return HttpResponseRedirect("/addaterrizaje/"+id_programa+"/")
    else:
        aterrizaje =Aterrizajes(pk=id)
