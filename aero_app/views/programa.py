from django.http import *
from django.shortcuts import render_to_response,redirect,render
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import *
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from aero_app.forms import ProgramaForm
from aero_app.models import *
from django.contrib.auth.decorators import permission_required



@login_required(login_url='/admin')
@permission_required('aero_app.add_programavuelo', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def createprograma(request,template_name='administracion/programas/new.html'):
    if request.method == "POST":
        form =ProgramaForm(request.POST)
        if form.is_valid():
            programa, created = ProgramaVuelo.objects.get_or_create(**form.cleaned_data) #esta funcion del orm de django lo que hace es verificar si el registo existe en base de datos y si no lo crea
            if(created == True):
                programa.save()
                return HttpResponseRedirect("/programas/")
            else:
                return render_to_response(template_name, {
                    'form': form,
                    'messages' : 'Error: ya existe el registro, por favor ingrese otro'
                }, context_instance=RequestContext(request))
    else:
        form =ProgramaForm()

    return render_to_response(template_name, {
        'form': form,
    }, context_instance=RequestContext(request))

@login_required(login_url='/admin')
@permission_required('aero_app.change_programavuelo', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def editprograma(request, id=None, template_name='administracion/programas/new.html'):
    if id:
        programa = get_object_or_404(ProgramaVuelo, pk=id)
    else:
        programa =ProgramaVuelo.objects.filter(pk=id)
    if request.POST:
        form =ProgramaForm(request.POST, instance=programa)
        if form.is_valid():
            form.save(commit=False)
            form.save()
            return HttpResponseRedirect("/programas/")
    else:
        form =ProgramaForm(instance=programa)

    return render_to_response(template_name, {
        'form': form,
    }, context_instance=RequestContext(request))

@login_required(login_url='/admin')
@permission_required('aero_app.change_programavuelo', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def deleteprograma(request, id = None):
    if id:
        programa = get_object_or_404(ProgramaVuelo, pk=id)
        programa.delete();
        return HttpResponseRedirect("/programas/")
    else:
        programa =Vuelo(pk=id)
