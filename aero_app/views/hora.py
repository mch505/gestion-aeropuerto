from django.http import *
from django.shortcuts import render_to_response,redirect,render
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import *
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from aero_app.forms import HorarioProgramaForm
from aero_app.models import *
from django.contrib.auth.decorators import permission_required

@login_required(login_url='/admin')
@permission_required('aero_app.add_horasprograma', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def addhora(request,id=None,template_name='administracion/programas/horas/add.html'):
    if request.method == "POST":
        form =HorarioProgramaForm(request.POST)
        if form.is_valid():
            hora = form.save(commit=False)
            hora.dia_id = id
            hora.save()
            horas = HorarioPrograma.objects.filter(dia_id = id).prefetch_related('dia')
            return render_to_response(template_name, {
                'form': form,
                'horas': horas
            }, context_instance=RequestContext(request))
    else:
        form = HorarioProgramaForm()

    horas = HorarioPrograma.objects.filter(dia_id = id).prefetch_related('dia')
    return render_to_response(template_name, {
        'form': form,
        'horas': horas
    }, context_instance=RequestContext(request))


@login_required(login_url='/admin')
@permission_required('aero_app.delete_horasprograma', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def deletehora(request, id = None,id_dia = None):
    if id:
        hora = get_object_or_404(HorarioPrograma, pk=id)
        hora.delete();
        form = HorarioProgramaForm()
        return HttpResponseRedirect("/addhora/"+id_dia+"/")
    else:
        hora =HorarioPrograma(pk=id)
