from django.http import *
from django.shortcuts import render_to_response,redirect,render
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import *
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from aero_app.forms import EscalaTecnicaFrom
from aero_app.models import *
from django.contrib.auth.decorators import permission_required

@login_required(login_url='/admin')
@permission_required('aero_app.add_escalastecnicas', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def addescalastecnica(request,id=None,template_name='administracion/programas/escalastecnicas/add.html'):
    if request.method == "POST":
        form =EscalaTecnicaFrom(request.POST)
        if form.is_valid():
            escalastecnica = form.save(commit=False)
            escalastecnica.programa_id = id
            escalastecnica.save()
            escalastecnicas = EscalasTecnicas.objects.filter(programa_id = id)
            return render_to_response(template_name, {
                'form': form,
                'escalastecnicas': escalastecnicas
            }, context_instance=RequestContext(request))
    else:
        form = EscalaTecnicaFrom()

    escalastecnicas = EscalasTecnicas.objects.filter(programa_id = id)
    return render_to_response(template_name, {
        'form': form,
        'escalastecnicas': escalastecnicas
    }, context_instance=RequestContext(request))


@login_required(login_url='/admin')
@permission_required('aero_app.delete_escalastecnicas', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def deleteescalastecnica(request, id = None,id_programa = None):
    if id:
        escalastecnica = get_object_or_404(EscalasTecnicas, pk=id)
        escalastecnica.delete();
        form = EscalaTecnicaFrom()
        return HttpResponseRedirect("/addescalastecnica/"+id_programa+"/")
    else:
        escalastecnica =EscalasTecnicas(pk=id)
