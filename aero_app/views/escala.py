from django.http import *
from django.shortcuts import render_to_response,redirect,render
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import *
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from aero_app.forms import EscalaForm
from aero_app.models import *
from django.contrib.auth.decorators import permission_required

@login_required(login_url='/admin')
@permission_required('aero_app.add_tipoescala', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def createescala(request,template_name='administracion/escalas/new.html'):
    if request.method == "POST":
        form = EscalaForm(request.POST)
        if form.is_valid():
            escala, created = TipoEscala.objects.get_or_create(**form.cleaned_data) #esta funcion del orm de django lo que hace es verificar si el registo existe en base de datos y si no lo crea
            if(created == True):
                escala.save()
                return HttpResponseRedirect("/escalas/")
            else:
                return render_to_response(template_name, {
                    'form': form,
                    'messages' : 'Error: ya existe el registro, por favor ingrese otro'
                }, context_instance=RequestContext(request))
    else:
        form = EscalaForm()

    return render_to_response(template_name, {
        'form': form,
    }, context_instance=RequestContext(request))

@login_required(login_url='/admin')
@permission_required('aero_app.change_tipoescala', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def editescala(request, id=None, template_name='administracion/escalas/new.html'):
    if id:
        escala = get_object_or_404(TipoEscala, pk=id)
    else:
        escala = TipoEscala.objects.filter(pk=id)
    if request.POST:
        form = EscalaForm(request.POST, instance=escala)
        if form.is_valid():
            form.save(commit=False)
            form.save()
            return HttpResponseRedirect("/escalas/")
    else:
        form = EscalaForm(instance=escala)

    return render_to_response(template_name, {
        'form': form,
    }, context_instance=RequestContext(request))


@permission_required('aero_app.delete_tipoescala', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def deleteescala(request, id = None):
    if id:
        escala = get_object_or_404(TipoEscala, pk=id)
        escala.delete();
        return HttpResponseRedirect("/escalas/")
    else:
        user = Escala(pk=id)
