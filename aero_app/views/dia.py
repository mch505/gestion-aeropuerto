from django.http import *
from django.shortcuts import render_to_response,redirect,render
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import *
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from aero_app.forms import DiasProgramaFrom
from aero_app.models import *
from django.contrib.auth.decorators import permission_required

@login_required(login_url='/admin')
@permission_required('aero_app.add_diasprograma', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def adddia(request,id=None,template_name='administracion/programas/dias/add.html'):
    if request.method == "POST":
        form =DiasProgramaFrom(request.POST)
        if form.is_valid():
            dia = form.save(commit=False)
            dia.programa_id = id
            dia.save()
            dias = DiasPrograma.objects.filter(programa_id = id)
            return render_to_response(template_name, {
                'form': form,
                'dias': dias
            }, context_instance=RequestContext(request))
    else:
        form = DiasProgramaFrom()

    dias = DiasPrograma.objects.filter(programa_id = id)
    return render_to_response(template_name, {
        'form': form,
        'dias': dias
    }, context_instance=RequestContext(request))


@login_required(login_url='/admin')
@permission_required('aero_app.delete_diasprograma', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def deletedia(request, id = None,id_programa = None):
    if id:
        dia = get_object_or_404(DiasPrograma, pk=id)
        dia.delete();
        form = DiasProgramaFrom()
        return HttpResponseRedirect("/adddia/"+id_programa+"/")
    else:
        dia =DiasPrograma(pk=id)
