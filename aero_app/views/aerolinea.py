from django.http import *
from django.shortcuts import render_to_response,redirect,render
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import *
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from aero_app.forms import AerolineaForm
from aero_app.models import *
from django.contrib.auth.decorators import permission_required

@login_required(login_url='/admin')
@permission_required('aero_app.add_aerolinea', login_url='/forbiden/') #esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def createaerolinea(request,template_name='administracion/aerolineas/new.html'):
    if request.method == "POST":
        form =AerolineaForm(request.POST)
        if form.is_valid():
            aerolinea, created = Aerolinea.objects.get_or_create(**form.cleaned_data) #esta funcion del orm de django lo que hace es verificar si el registo existe en base de datos y si no lo crea
            if(created == True):
                aerolinea.save()
                return HttpResponseRedirect("/aerolineas/")
            else:
                return render_to_response(template_name, {
                    'form': form,
                    'messages' : 'Error: ya existe el registro, por favor ingrese otro'
                }, context_instance=RequestContext(request))
    else:
        form =AerolineaForm()

    return render_to_response(template_name, {
        'form': form,
    }, context_instance=RequestContext(request))

@login_required(login_url='/admin')
@permission_required('aero_app.change_aerolinea', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def editaerolinea(request, id=None, template_name='administracion/aerolineas/new.html'):
    if id:
        # get_object_or_404 obtiene el objeto del modelo que se le pase y si no encuentra nada devuelve un error 404 (Not Found)
        aerolinea = get_object_or_404(Aerolinea, pk=id)
    else:
        aerolinea =Aerolinea.objects.filter(pk=id)

    if request.POST:
        form =AerolineaForm(request.POST, instance=aerolinea) #insctance es la instancia del objeto que se obtuvo de la base de datos e indica a django que debe hace un update
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("/aerolineas/")
    else:
        form =AerolineaForm(instance=aerolinea)

    return render_to_response(template_name, {
        'form': form,
    }, context_instance=RequestContext(request))

@login_required(login_url='/admin')
@permission_required('aero_app.delete_aerolinea', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def deleteaerolinea(request, id = None):
    if id:
        aerolinea = get_object_or_404(Aerolinea, pk=id)
        aerolinea.delete()
        return HttpResponseRedirect("/aerolineas/")
    else:
        aerolinea =Aerolinea(pk=id)
