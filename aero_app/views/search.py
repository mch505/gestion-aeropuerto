# -*- encoding: utf-8 -*-
from django.http import *
from django.shortcuts import render_to_response,redirect,render
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.core.context_processors import csrf
from django.contrib import auth
from django.contrib.auth import logout
from aero_app.models import *
from datetime import datetime
from django.db.models import Prefetch


def main(request):
    c = {}
    c.update(csrf(request))
    aeropuertos = Aeropuerto.objects.all()
    clases = Clase.objects.all()
    context = {'aeropuertos': aeropuertos,'clases': clases}
    return render(request,'webapp/main.html',context)

def get_vuelos(request):
     if request.GET:
        fechadel = datetime.strptime(request.GET["del"], '%d/%m/%Y').date()
        fechaal = datetime.strptime(request.GET["al"], '%d/%m/%Y').date()
        id_aeropuerto1 = request.GET["aero1"]
        id_aeropuerto2 = request.GET["aero2"]
        clase = request.GET["clase"]
        cantidad = int(float(request.GET["cantidad"]))

        vuelo1 = Vuelos.objects.filter(fecha_vuelo = fechadel).prefetch_related("programas")
        vuelo2 = Vuelos.objects.filter(fecha_vuelo = fechaal).prefetch_related("programas")

        aerosIda = []
        aerosVuelta = []
        try:
            for vuelo in vuelo1.all():
                for programa in vuelo.programas.all():
                    despegues = programa.despegues.filter(aeropuerto_id = id_aeropuerto1).first()
                    aterrizajes = programa.aterrizajes.filter(aeropuerto_id = id_aeropuerto2).first()
                    try:
                        precio = float(programa.precios.filter(clase_id = clase).first().precio) * cantidad
                    except:
                        precio = 0
                    if despegues.aeropuerto.id == int(float(id_aeropuerto1)) and aterrizajes.aeropuerto.id == int(float(id_aeropuerto2)):
                        horarios = ""
                        for dia in programa.dias.all():
                            horarios_dia = ""
                            for horas in dia.horas.all():
                                horarios_dia = str(horas.hora)[:5]+" , "+horarios_dia
                            horarios = dia.dias_semana+" ["+horarios_dia+"] "+horarios+"  "
                        if precio > 0:
                            aerosIda.append({
                                'aeropuerto_ida' : despegues.aeropuerto.nombre,
                                'aeropuerto_vuelta' : aterrizajes.aeropuerto.nombre,
                                'numero_vuelo': vuelo.numero_vuelo,
                                'aerolinea': programa.aerolinea.nombre,
                                'dias': horarios,
                                'cantidad': cantidad,
                                'precio': precio
                            })
            for vuelo in vuelo2.all():
                for programa in vuelo.programas.all():
                    despegues = programa.despegues.filter(aeropuerto_id = id_aeropuerto2).first()
                    aterrizajes = programa.aterrizajes.filter(aeropuerto_id = id_aeropuerto1).first()
                    try:
                        precio = float(programa.precios.filter(clase_id = clase).first().precio) * cantidad
                    except:
                        precio = 0
                    if despegues.aeropuerto.id == int(float(id_aeropuerto2)) and aterrizajes.aeropuerto.id == int(float(id_aeropuerto1)):
                        horarios = ""
                        for dia in programa.dias.all():
                            horarios_dia = ""
                            for horas in dia.horas.all():
                                horarios_dia = str(horas.hora)[:5]+" , "+horarios_dia
                            horarios = dia.dias_semana+" ["+horarios_dia+"] "+horarios+"  "
                        if precio > 0:
                            aerosVuelta.append({
                                'aeropuerto_ida' : despegues.aeropuerto.nombre,
                                'aeropuerto_vuelta' : aterrizajes.aeropuerto.nombre,
                                'numero_vuelo': vuelo.numero_vuelo,
                                'aerolinea': programa.aerolinea.nombre,
                                'dias': horarios,
                                'cantidad': cantidad,
                                'precio': precio
                            })

            if len(aerosIda) == 0 or len(aerosVuelta) == 0:
                context = {'ida': [], 'vuelta': [], "messages": "No hubieron resultados para su búsqueda por favor intente con otros parámetros"}
            else:
                context = {'ida': aerosIda, 'vuelta': aerosVuelta}
        except:
            context = {'ida': [], 'vuelta': [], "messages": "No hubieron resultados para su búsqueda por favor intente con otros parámetros"}

        return render(request,'webapp/results.html',context)
