from django.http import *
from django.shortcuts import render_to_response,redirect,render
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import *
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from aero_app.forms import AvionForm
from aero_app.models import *
from django.contrib.auth.decorators import permission_required

@login_required(login_url='/admin')
@permission_required('aero_app.add_modeloavion', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def createavion(request,template_name='administracion/aviones/new.html'):
    if request.method == "POST":
        form =AvionForm(request.POST)
        if form.is_valid():
            avion, created = ModeloAvion.objects.get_or_create(**form.cleaned_data)
            if(created == True):
                avion.save()
                return HttpResponseRedirect("/aviones/")
            else:
                return render_to_response(template_name, {
                    'form': form,
                    'messages' : 'Error: ya existe el registro, por favor ingrese otro'
                }, context_instance=RequestContext(request))
    else:
        form =AvionForm()

    return render_to_response(template_name, {
        'form': form,
    }, context_instance=RequestContext(request))


@login_required(login_url='/admin')
@permission_required('aero_app.change_modeloavion', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def editavion(request, id=None, template_name='administracion/aviones/new.html'):
    if id:
        avion = get_object_or_404(ModeloAvion, pk=id)
    else:
        avion =ModeloAvion.objects.filter(pk=id)
    if request.POST:
        form =AvionForm(request.POST, instance=avion)
        if form.is_valid():
            form.save(commit=False)
            form.save()
            return HttpResponseRedirect("/aviones/")
    else:
        form =AvionForm(instance=avion)

    return render_to_response(template_name, {
        'form': form,
    }, context_instance=RequestContext(request))



@login_required(login_url='/admin')
@permission_required('aero_app.delete_modeloavion', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def deleteavion(request, id = None):
    if id:
        avion = get_object_or_404(ModeloAvion, pk=id)
        avion.delete();
        return HttpResponseRedirect("/aviones/")
    else:
        avion =Avion(pk=id)
