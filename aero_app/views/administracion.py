from django.http import *
from django.shortcuts import render_to_response,redirect,render
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import *
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from aero_app.models import *
from datetime import datetime
from django.contrib.auth.decorators import permission_required

@login_required(login_url='/admin')
def home(request):
    aeropuerto = ''
    aeropuerto = Perfil.objects.filter(user_id = request.user.id).prefetch_related('user').first() #obtiene los datos del empleado que se loguea
    vuelo = Vuelos.objects.all().count
    programas = ProgramaVuelo.objects.all().count
    aerolinea = Aerolinea.objects.all().count
    paises = Pais.objects.all().count
    return render(request,'administracion/base.html', {'aeropuerto': aeropuerto,'vuelos':vuelo,'programas':programas,'aerolinea':aerolinea,'pais':paises})

def forbiden(request):
    return render(request,'administracion/forbiden.html')


@login_required(login_url='/admin')
@permission_required('aero_app.view_aeropuerto', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def aeropuertos_list(request):
    aeropuertos = Aeropuerto.objects.all()
    context = {'aeropuertos': aeropuertos}
    return render(request,'administracion/aeropuerto/aeropuerto_list.html', context)

@login_required(login_url='/admin')
@permission_required('aero_app.view_pais', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def paises_list(request):
    paises = Pais.objects.all()
    context = {'paises': paises}
    return render(request,'administracion/paises/pais_list.html', context)

@login_required(login_url='/admin')
@permission_required('aero_app.view_ciudad', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def ciudades_list(request):
    ciudad = Ciudad.objects.all().prefetch_related('pais').all()
    context = {'ciudades': ciudad}
    return render(request,'administracion/ciudades/ciudad_list.html', context)

@login_required(login_url='/admin')
@permission_required('aero_app.view_aerolinea', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def aerolineas_list(request):
    aerolinea = Aerolinea.objects.all().prefetch_related('pais').all()
    context = {'aerolineas': aerolinea}
    return render(request,'administracion/aerolineas/aerolinea_list.html', context)

@login_required(login_url='/admin')
@permission_required('aero_app.view_modeloavion', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def aviones_list(request):
    avion = ModeloAvion.objects.all()
    context = {'aviones': avion}
    return render(request,'administracion/aviones/avion_list.html', context)

@login_required(login_url='/admin')
@permission_required('aero_app.view_vuelos', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def vuelos_list(request):
    vuelo = Vuelos.objects.all()
    context = {'vuelos': vuelo}
    return render(request,'administracion/vuelos/vuelo_list.html', context)

@login_required(login_url='/admin')
@permission_required('aero_app.view_tipoescala', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def escalas_list(request):
    escala = TipoEscala.objects.all()
    context = {'escalas': escala}
    return render(request,'administracion/escalas/escalas_list.html', context)

@login_required(login_url='/admin')
@permission_required('aero_app.view_programavuelo', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def programas_list(request):
    programas = ProgramaVuelo.objects.all().prefetch_related('vuelo','aerolinea','despegues','aterrizajes','escalas','dias')
    context = {'programas': programas}
    return render(request,'administracion/programas/programa_list.html', context)

@login_required(login_url='/admin')
@permission_required('aero_app.view_clase', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def clases_list(request):
    clases = Clase.objects.all()
    context = {'clases': clases}
    return render(request,'administracion/clases/clase_list.html', context)
