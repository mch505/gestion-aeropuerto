from django.shortcuts import render, redirect, get_object_or_404
from aero_app.forms import ProfileCreateForm
from aero_app.models import Profile
from django.contrib import messages
from django.contrib.auth.decorators import login_required


@login_required
def view_profile(request):
    try:
        if request.method == 'POST':
            form = ProfileCreateForm(request.POST)
            if form.is_valid():
                user_profile = form.save(commit=False)
                request.user.profile.address = user_profile.address
                request.user.profile.apt = user_profile.apt
                request.user.profile.city = user_profile.city
                request.user.profile.first_name = user_profile.first_name
                request.user.profile.last_name = user_profile.last_name
                request.user.profile.payment = user_profile.payment
                request.user.profile.state = user_profile.state
                request.user.profile.zip_code = user_profile.zip_code
                request.user.profile.save()
                messages.add_message(request, messages.INFO, 'Your profile has been updated.')
                return redirect('profile')
        else:
            if request.user.profile:
                user_profile = get_object_or_404(Profile, pk=request.user.profile.id)
                form = ProfileCreateForm(instance=user_profile)
                context = {'form': form}
                return render(request, 'customers/profile.html', context)
    except Profile.DoesNotExist:
        return redirect('profile_create')
    except Exception as e:
        messages.add_message(request, messages.ERROR, 'Unexpected error: {}.'.format(e))
        return redirect('home')
