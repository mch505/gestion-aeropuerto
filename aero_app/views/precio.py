from django.http import *
from django.shortcuts import render_to_response,redirect,render
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import *
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from aero_app.forms import PrecioForm
from aero_app.models import *
from django.contrib.auth.decorators import permission_required

@login_required(login_url='/admin')
@permission_required('aero_app.add_preciovuelo', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def addprecio(request,id=None,template_name='administracion/programas/precios/add.html'):
    if request.method == "POST":
        form =PrecioForm(request.POST)
        if form.is_valid():
            precio = form.save(commit=False)
            precio.programa_id = id
            precio.save()
            precios = PrecioVuelo.objects.filter(programa_id = id)
            return render_to_response(template_name, {
                'form': form,
                'precios': precios
            }, context_instance=RequestContext(request))
    else:
        form = PrecioForm()

    precios = PrecioVuelo.objects.filter(programa_id = id)
    return render_to_response(template_name, {
        'form': form,
        'precios': precios
    }, context_instance=RequestContext(request))

@login_required(login_url='/admin')
@permission_required('aero_app.delete_preciovuelo', login_url='/forbiden/')#esta linea verifica que el usuario tenga los permisos adecuados para acceder a la funcionalidad
def deleteprecio(request, id = None,id_programa = None):
    if id:
        precio = get_object_or_404(PrecioVuelo, pk=id)
        precio.delete();
        form = PrecioForm()
        return HttpResponseRedirect("/addprecio/"+id_programa+"/")
    else:
        precio =precios(pk=id)
