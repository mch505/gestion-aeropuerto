from django.contrib import admin
from aero_app.models import Perfil
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User



# Register your models here.
class PerfilInline(admin.StackedInline):
    model = Perfil
    can_delete = False
    verbose_name_plural = 'perfil'

# Define a new User admin
class UserAdmin(UserAdmin):
    inlines = (PerfilInline, )
    list_display = ('username', 'first_name', 'last_name', 'perfil')


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
